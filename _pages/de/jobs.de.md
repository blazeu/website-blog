---
layout: page
permalink: /jobs/
lang: de
---

# Jobs

Wenn Du daran interessiert bist, bei ViperDev zu arbeiten, schreib uns einfach eine E-Mail an
[jobs@viperdev.io](mailto:jobs@viperdev.io) mit einigen Informationen über Dich.

Achte darauf, die E-Mail auf Englisch zu formulieren. Du wirst durch ein internationales Team beurteilt.

Hier sind ein paar Fragen, die Du vielleicht beantworten möchtest:

- Was motiviert Dich, bei ViperDev zu arbeiten?
- Was wäre Dein idealer Job?
     - Wo ist er?
     - Was tust Du da genau?
     - Vollzeit, Teilzeit, Praktikant, Auftragnehmer?
- Was glaubst Du, fehlt uns als Unternehmen?
- Möchtest Du mit der Gründung Deiner eigenen Firma beginnen? Ist etwas in Vorbereitung?

# Stellenangebote

Sales Manager/In
 - Wir suchen derzeit einen Sales Manager/in. Sie können die Stellenanzeige
<a>
<a href="/assets/StellenanzeigeSales.pdf">hier</a>
anzeigen.
