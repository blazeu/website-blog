---
layout: post
title:  "The Right Way to Define Features for an MVP"
date:   2018-04-25 2:52:21 -0500
author: "Raluca Cîrjan"
profile_link: "https://www.linkedin.com/in/raluca-cirjan/"
categories: Startup
lang: en
image:
  feature: start_up_dock.png
  credit: "Freepik.com"
---

Entrepreneurs often fall into the trap of investing too much time and money in non essential features for their product. When the startup simply needs to validate the most valuable idea, so how can you define features for your Minimum Viable Product?

### Start with ‘Why?’
Most companies think about their products only in terms of features and statistics. Simon Sinek argues that the most successful brands start by explaining why their product matters. Ask yourself why are you creating the product and why should people care enough to buy it. This is a great starting point to define the features you need for your MVP. It gives you the chance to realize **what differentiates your product from others** and what features to focus on in order to make those qualities stand out. 

The co-founders of Dropbox, for instance, believed that file synchronization was a problem that most people were not aware of. They assumed that once people experienced the solution, they would not be able to imagine how they ever lived without it. To **avoid the risk of investing years of hard work and money** only to create a product nobody wanted, Drew Houston, co-founder of Dropbox, made a three minute video demonstrating how the technology worked. This simple idea validated the assumption that customers wanted the product. It drove hundreds of thousands of people to their website. This perfectly illustrates that starting with ‘Why?’ instead of obsessing over features can lead to the validation of your idea.

### Prioritize your features
Identifying the core features can be a challenging task. Approach prioritization as a team activity to get different perspectives. To make the process easier, you can start by using the **MoSCoW method**, which is an acronym derived from the first letter of each of the prioritization categories: Must have, Should have, Could have, and Won’t have. If you are building an innovative product, the must have features are sufficient to demonstrate that it solves the core problem you have identified. Let’s take an invoicing app as an example: 

* **Must have** - Think of the *must have* features as the backbone of your MVP. For an invoicing app, a must have feature is being able to generate invoices and store them.
* **Should have** - These features are necessary, but aren't essential for your early-stage iteration. Having the possibility to work on invoices in the Cloud or sending the invoice to customers by email are some examples of should have features.
* **Could have** - You *could have* perfectly designed features and extra-functionalities, but it’s best to put them in the drawer until you validate your idea. For instance: integrating the app with other existing business applications.
* **Won't have** - Your MVP should *not* have any features that don’t contribute directly to your goal. E.g.: Role-based access control, allowing companies to choose how different types of employees interact with the invoices.

### Test, learn and adapt
An MVP provides valuable feedback to guide future development. It’s important to keep in mind that during its development cycle, an MVP will require various changes. You will have the opportunity to implement all of the *could have* features that are still waiting in the drawer. The complete set of features should only be designed and developed after considering feedback from the initial users. Also keep in mind that **gathering insights from an MVP** might be less expensive than developing a product with more features.

In order to define the features for your MVP the right way, make sure to:
* Start with ‘why?’
* Prioritize your features
* Test, learn and adapt

Need help in defining your features? [Book a call](https://calendly.com/viper/kickstart/) with us. You might also enjoy reading [3 Reasons Why You Should Build an MVP](https://viperdev.io/startup/2018/04/18/3-reasons-why-you-should-build-an-mvp). Join the comment section and let us know what you think.
