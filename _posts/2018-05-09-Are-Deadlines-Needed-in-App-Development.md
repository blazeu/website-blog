---
layout: post
title:  "Are Deadlines Needed in App Development?"
date:   2018-05-09 2:52:21 -0500
author: "Raluca Cîrjan"
profile_link: "https://www.linkedin.com/in/raluca-cirjan/"
categories: Startup
lang: en
image:
  feature: time_for_business.png
  credit: "Freepik.com"
---

Deadlines are a core part of project management, but what is the best way to plan the process when building an app? Despite numerous benefits, target dates can become the cause of a lot of stress if set improperly, so take these steps into consideration to help your developer meet the deadline:

### Set realistic expectations
In software development, some things are beyond the IT team’s control. The project manager’s lack of involvement, frequent task changes or the absence of clear goals are just a few things that can influence their schedule. Despite these factors, the app’s owner needs to know an estimated release date. One solution for setting a realistic deadline would be to **ask every member of the team to give estimates**, then to also add a few days or weeks, depending on the app’s complexity, to cover the unexpected events that might occur. While this method may offer developers a reasonable amount of time, **keep in mind that long deadlines fail to motivate**, which is the reason why you also should divide the project into smaller phases.

### Identify the milestones
Milestones are small steps that lead the team to the ultimate goal: building a functional app. Try to **space them properly** by not falling into trap of labeling each task completion as a milestone. For instance, if you are building an e-commerce app, your developer can know the target date for integrating the payment process, but does not have to mark as a milestone each user interface element that is necessary to perform an action, such as a button. Keep in mind that developers prefer organizing their activity into sprints, so try being open-minded, as lack of flexibility can lead to lack of quality. In addition, consider creating a [Program Evaluation Review Technique chart](https://en.wikipedia.org/wiki/Program_evaluation_and_review_technique). PERT charts show the relationship between project milestones. This way, your developer knows what milestones must be completed before starting other activities or what tasks can be done in parallel.

### Use feedback to adapt
Meetings are necessary to check the progress, **provide timely feedback** and answer your developer’s questions, but keep them short and productive. Remind him of the established milestones in order to remain on the same page. If your developer is behind the schedule, make sure to talk about the causes and what can be improved or avoided. If important deadlines are missed due to factors that are beyond your control, consider the option of paying overtime or giving up on some unessential features. The flexibility to **adapt and change** as new challenges arise is a key component of building a successful app.

While giving estimates and setting milestones with your app developer is important in order to work more efficiently and not waste resources, it is not always a smooth process. Whether you prefer deadlines or organizing the activity into sprints, make sure to:
* Set realistic expectations
* Identify the milestones
* Use feedback to adapt

Want to [turn your business idea into an app?](https://viperdev.io/startup/2018/05/03/turn-your-business-idea-into-an-app) Feel free to [book a call](https://calendly.com/viper) with us.
