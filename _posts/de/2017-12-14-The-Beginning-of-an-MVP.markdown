---
layout: post
title:  "Der Anfang eines MVPs"
date:   2017-12-14 18:52:21 -0500
author: "Lasse Schuirmann"
profile_link: "https://www.linkedin.com/in/lasse-schuirmann-5395a7123/"
categories: Startup
lang: de
image:
  feature: rocket.png
  credit: "Photoroyalty — Freepik.com"
---

[Viper Development](https://viperdev.io) ist ein Softwareentwicklungsunternehmen, das Startups dabei hilft, ihre App innerhalb kürzester Zeit mit einem knappen Budget in den App-Store zu bringen.

### Wofür ist dieser Blog?

Dieser Blogbeitrag ist eine kleine FAQ über das, was wir hier anfangen wollen. Da wir MVPs machen, ist er ein MVP - wir werden weitere Blogbeiträge über einige Projekte, die wir bereits gemacht haben, wie Auftragnehmer mit uns arbeiten, wie wir arbeiten und was auch immer anliegt für euch bereithalten. Lass uns wissen, was Du sonst noch lesen möchtest :)

### Was tun wir?

Es ist einfach. Wir erstellen normalerweise Apps ab 3000 € innerhalb eines Monats.

Mit jedem Projekt verbessern wir unser Basisprojektdesign, um jedem unserer Kunden einen noch besseren Start in jedes Projekt zu ermöglichen und dabei noch mehr zum gleichen Preis zu liefern.

### Warte, 3000 €? Für alles?

Ja. Nun, nein. Hier sind einige Einschränkungen.

1. *Dies gilt für Startups und kleine Unternehmen.* Wir unterstützen Dein Wachstum. Wir sind begeisterte Gründer und wollen Dich zum Erfolg führen. Wenn Du ein größeres Unternehmen bist, berechnen wir Dir mehr, damit wir kleinere Unternehmen besser unterstützen können.
2. Dies gilt für ein MVP. *Sehr einfach* Wir prüfen gemeinsam mit Dir Deine/Eure Geschäftsziele und stellen sicher, dass wir das wichtigste Alleinstellungsmerkmal zu diesem Preis liefern. Das bedeutet, dass Du Funktionen so lange kündigen kannst, bis sie dem Budget entsprechen. Oft entscheiden sich Gründer jedoch für zusätzliche Features, die ein bisschen mehr extra kosten. Wir schaffen es jedoch oft zu einem Preis um 5000 Euro, der ein paar zusätzliche Funktionen abdeckt.

### Und wer ist das Team?

Gute Frage - und ich denke, das ist eine gute Idee für einen zukünftigen Blogbeitrag :)

Kurz gesagt: Wir sind eine Gruppe von Gründern (für Projektmanagement und Design) und eine Reihe von Entwicklern, die hart an ein paar Open-Source-Projekten gearbeitet haben und von neuen, spaßigen Projekten begeistert sind

### Unsere Grundwerte

Werte definieren uns als Menschen und als Unternehmen. Dafür stehen wir:

#### Ansprechbar

Wir machen Deine App innerhalb von Tagen möglich - nicht Monate, keine Jahre. Wir antworten innerhalb von Stunden - nicht Wochen. Ein Startup muss schnell und agil sein und wir sind dazu da, um Sie zu beschleunigen, nicht zu verlangsamen.

#### Inkrementell

Wir müssen schlank sein. Wir müssen schnell sein. Wir müssen die Dinge, die wir bauen und tun, iterieren. Wir bauen nicht das komplette Produkt nach dem “Wasserfall-Modell” auf einmal. Wir lernen und passen uns dem Weg an.

#### Modern

Schnell und inkrementell zu sein, ist alles gut und gut, aber wenn wir unseren Kunden ein wettbewerbsfähiges Produkt anbieten wollen, müssen wir moderne Technologien einsetzen.

Wir sind kein einzelner Entwickler, sondern ein Netzwerk von internationalen Entwicklern und Gründern und wir sind bestrebt, uns über die neuesten und besten Innovationen, die es gibt, auf dem Laufenden zu halten.

[Docker](https://docker.com){:target="_blank"}, [ionic](https://ionicframework.com/){:target="_blank"}, [PWAs](https://www.wikiwand.com/en/Progressive_web_app){:target="_blank"} — Wir stellen immer sicher, dass wir die neuesten und besten Dinge verwenden, um schnell, skalierbar und effizient zu sein, ohne unsere Kunden an einen bezahlten oder proprietären Service oder eine Software zu binden.

#### Transparent

Wir kommen aus einem Open-Source-Hintergrund. Wir lieben Transparenz und Offenheit. Möchtest du unsere Verträge sehen? [Schaue sie dir an.](https://gitlab.com/viperdev/open/legal/){:target="_blank"} Möchtest du sehen, wie wir den Kult des ["Party-Papageis"](http://cultofthepartyparrot.com/){:target="_blank"} in unser mattermost (eine slack Alternative) integrieren? [Hier bitte klicken.](https://gitlab.com/viperdev/open/mattermost-emoji-importer){:target="_blank"} Want to talk to us? [Join our mattermost.](https://mattermost.gitmate.io/signup_user_complete/?id=9ecq3njoxfgmfft4uq9g35pejy){:target="_blank"} Transparenz ist der Schlüssel für eine gute Kundenbeziehung. Wenn Du ein Kunde bist, werden wir Dich in die äußerste Randlage ziehen, so dass Du eine direkte Verbindung zu unseren Gründern, unseren Entwicklern und unserem Netzwerk hast.

### Was liegt vor uns?

Wir haben gerade erst angefangen. Wenn Du mehr über uns wissen möchtest, woher wir kommen und wohin wir gehen, hinterlasse uns einen Kommentar, über das, woran Du am meisten interessiert bist und [trete unserem mattermost Chat bei!](https://mattermost.gitmate.io/signup_user_complete/?id=9ecq3njoxfgmfft4uq9g35pejy){:target="_blank"}

Also, was liegt vor uns? Wir wissen es auch noch nicht so genau. Aber wir schauen dem Kommenden zuversichtlich entgegen :)
