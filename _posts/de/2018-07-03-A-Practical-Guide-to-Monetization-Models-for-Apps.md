---
layout: post
title:  "Praxis-Leitfaden zur Monetarisierung von Apps"
date:   2018-07-03 2:52:21 -0500
author: "Cîrjan Raluca"
profile_link: "https://www.linkedin.com/in/raluca-cirjan/"
categories: Startup
lang: de
image:
  feature: monetize.png
  credit: "Freepik.com"
---

Durch die Erstellung einer Monetarisierungsstrategie, die für Deine App am besten geeignet ist, kommst Du Deinen Geschäftszielen einen Schritt näher. Bevor Du **Monetarisierungsmodelle** untersuchst, denke daran, dass Du [immer mit dem "Warum?" beginnen solltest](https://viperdev.io/startup/2018/04/25/the-right-way-to-define-features-for-an-mvp).

Warum sollte ein Nutzer ein Abonnement kaufen oder bezahlen, um eine App herunterzuladen? Dies gibt ihnen die Möglichkeit zu erkennen, was Deine App von anderen unterscheidet, was Menschen brauchen und auf welche Funktionen sie sich konzentrieren müssen, um deren Bedürfnisse zu erfüllen. **Die Monetarisierungsstrategie sollte immer im Hinblick auf den Endbenutzer betrachtet werden.**

Hier sind sechs App-Monetarisierungsstrategien, die es wert sind, sich einmal genauer anzuschauen:

1. **In-App-Werbung**  
Dies ist eine der beliebtesten App-Monetarisierungsstrategien. Da Menschen mehr Zeit mit ihrem Smartphone verbringen als mit jedem anderen Gerät, sind Unternehmen daran interessiert, ihre Werbebudgets mobilen Apps zuzuordnen. Banner, Videos mit automatischer Wiedergabe oder gesponserte Beiträge können in einer mobilen App angezeigt werden. Es ist von entscheidender Bedeutung, sicherzustellen, dass eine Anzeige für das Publikum relevant ist, wenn sie nicht-invasiv präsentiert wird, da dies dazu führen kann, dass die Nutzererfahrung insgesamt beeinträchtigt wird.  
Einer der Vorteile dieser Technik zur Generierung von Einnahmen durch Deine App besteht darin, dass die App für den Nutzer kostenlos bleibt, während Du demografische Daten und Verhaltensdaten erfasst, die kannst Du verschiedene Werbenetzwerke kombinieren.
2. **Bezahlte Apps**  
Mit diesem Monetarisierungsmodell können Apps nur heruntergeladen werden, wenn der Nutzer sie aus dem App Store kauft. Wenn Du planst, Einnahmen durch Deine App zu erzielen, indem Du sie verkaufst, dann solltest Du unbedingt hervorheben, was an Deinem Produkt einzigartig ist gegenüber ähnlichen Apps, die kostenlos zur Verfügung stehen. Mit diesem Modell erhältst Du 70% des Gewinns und die App-Stores behalten 30%. Diese Strategie kann eine Herausforderung darstellen, da die Nutzer sie häufig als Hindernis für das Herunterladen Deiner App betrachten, insbesondere da sie so viele kostenlose andere Optionen zur Verfügung stellen. Auf der anderen Seite, wenn Du eine starke Marketing-Strategie hast und Dein Produkt einen Mehrwert gegenüber kostenlosen, ähnlichen Optionen bietet, ist ein kostenpflichtiges App-Modell eine Überlegung wert.
3. **Patenschaften**  
Dieses Modell erfordert, dass Deine Zielgruppe groß genug ist, um Marken für Werbung zu gewinnen. Die Art und Weise, wie Sponsoren arbeiten, besteht darin, dass Werbetreibende Deinen Nutzern Belohnungen anbieten, wenn sie bestimmte In-App-Aktionen abschließen. Dieses Modell eignet sich besonders für Apps, die sich auf bestimmte Branchen wie z. B. Event-Apps konzentrieren.  
Der Nachteil besteht darin, dass der Umsatz auf den Wert der Partnerschaftsvereinbarung beschränkt ist und man normalerweise nicht mehr verdient, wenn die Kampagne besser als erwartet abschneidet. Der gute Teil ist, dass dieses Modell von den Nutzern normalerweise gut angenommen wird, solange der Inhalt relevant und wertvoll ist. Es empfiehlt sich, Sponsoring zu erwägen, nachdem eine starke Zielgruppe aufgebaut wurde, die einen Wert für einen Sponsor darstellt.
4. **In-App Käufe**  
Bei dieser Strategie werden physische oder virtuelle Güter über Deine App verkauft, um Gewinne zu erzielen. Sie ist am besten für die Spieleindustrie geeignet und einer Deiner Vorteile ist, dass Du dieses Monetarisierungsmodell mit anderen kombinieren kannst. Die App bleibt frei, während Benutzer wählen können, ob sie die Produkte kaufen oder nicht.
Du hast die Möglichkeit, sowohl virtuelle Waren, wie Lebensenergie für eine Figur in einem Spiel, als auch physische Produkte wie T-Shirts, Aufkleber oder jede Art von Markenware zu verkaufen. Die meisten App-Marktplätze berechnen 30% vom Wert des Produktes, aber der Gewinn ist immer noch hoch, besonders wenn es um virtuelle Güter geht.
5. **Freemium**  
Freemium, ein hybrides Modell zwischen kostenlosen und Premium-Apps, ist eine bekannte Strategie, bei der die App kostenlos zur Verfügung steht. Einige Funktionen sind jedoch gesperrt und der Nutzer kann sie durch Zahlung einer bestimmten Menge Geld freischalten. Das kann alles sein: Von der Freischaltung einer werbefreien Version bis hin zum Entsperren von Tools, neuen Levels, Filtern oder Speicherplatz.  
Wenn die Premium-Funktionen die Bedürfnisse des Nutzers erfüllen, zahlen Nutzer in der Regel dafür, die volle Funktionalität der App zu erleben. Viele Entwickler verfolgen diese spezielle Strategie, weil sie den Benutzern den Wert Deines Produkts beweisen können, ohne zuerst nach Geld zu fragen. Allerdings ist das Gleichgewicht der Schlüssel - denn zu viel zu geben, wird die Nutzer davon abhalten, Extras zu kaufen, während unzureichende kostenlose Funktionen Frustration verursachen können.
6. **Abonnements**  
Obwohl in gewisser Hinsicht dem Freemium-Modell ähnlich, können Benutzer es bei Abonnements versuchen, bevor sie kaufen. Sie haben die Möglichkeit, eine bestimmte Menge an Inhalten zu sehen, aber wenn sie weiterhin Zugriff darauf haben möchten, werden sie für den Inhalt, der in einer App bereitgestellt wird, monatlich belastet.  
Dieses Modell ist jedoch für keine Produktart geeignet. Die App muss die Art von Inhalten bereitstellen, die häufige und wiederholte Verwendung fördert. Es funktioniert gut für Unterhaltung, Nachrichten und Lifestyle (z. B. Netflix, The New York Times, Babbel).  


Wenn Du diese Modelle für die Monetarisierung einer App verstehst und sogar kombinierst, kannst Du eine eigene Strategie entwickeln, um mit Deiner App den größtmöglichen Return on Investment zu erzielen. Zum Beispiel kann eine kostenpflichtige App In-App-Käufe anbieten, um die Monetarisierung zu verbessern. Du kannst immer zwischen folgenden Monetarisierungsmodellen wählen:  

* In-App-Werbung
* Bezahlte / Premium Apps
* Patenschaften
* In-App Käufe
* Freemium
* Abonnements  

Bist Du bereit, [Deine Geschäftsidee in eine App umzuwandeln?](https://viperdev.io/startup/2018/05/03/turn-your-business-idea-into-an-app) Wenn Du beim Start Deiner App Hilfe benötigst, beraten wir Dich gerne zu den technischen Anforderungen und der Strategie Deiner App. [Buche jetzt einen Anruf mit uns](https://calendly.com/viper).