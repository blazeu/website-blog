---
layout: post
title:  "3 Reasons Why You Should Build an MVP"
date:   2018-04-18 2:52:21 -0500
author: "Raluca Cîrjan"
profile_link: "https://www.linkedin.com/in/raluca-cirjan/"
categories: Startup
lang: en
image:
  feature: rocket_plain.png
  credit: "Freepik.com"
---

Starting with a Minimum Viable Product is a great way to bring your product to market sooner, which gives you valuable feedback, and allows you to do all of this on a tight budget. **An MVP should only have the features that are essential to satisfying the value proposition.** Of course, there isn’t a one-size-fit-all MVP strategy. You will have to adapt depending on the industry you choose, the problem you’re solving and the existing competition.

### Building a successful MVP is the best way to fund the full project.

**Investors** will usually need to see **traction** before they are willing to put their money in. Let's take a look at Airbnb. It all started in 2007, when Brian Chesky and Joe Gebbia decided to use their living room as cheap accommodation for some design conference attendees who had lucked out on nearby hotels. They took pictures of their apartment, uploaded them on a simple website, and soon they had 3 paying guests: a woman from Boston, a man from India, and a father of four from Utah. In early 2009 they received $20,000 from Paul Graham, the co-founder of Y Combinator. This led to a further $600,000 from venture capitalists and the rest is history.

### Avoid paying excessive development costs by keeping things minimal.

Development companies will be delighted to rack up a long list of billable hours. Even your CTO will be very likely to code appealing but nonessential features - not with bad intentions, but because it’s exciting to build and less fun to figure out **what the market wants.** What does keeping things minimal mean? Brian Chesky and Joe Gebbia, the Airbnb co-founders created a simple solution to test demand by uploaded pictures on a simple website at the right time – before an important conference. They managed to validate their model **without obsessing over features.** Once they realized that customers only needed to see the flat and the price to make the deal, they knew that they could reach investors.

### Get your first paying clients. This first iteration will give you the financial boost for the next phase.

Your first clients will help you take the next steps not only financially, but also in terms of what your possible **target market** could be. Reaching out to early adopters who will want to use your first version is anything but easy, especially if you’re targeting a completely untapped market. This is why focusing on one **customer segment** and helping those customers solve their problems as if they’re your best friends will help you improve your product. Make them feel valuable by asking for their analysis and feedback on the product and user experience. The process involves measurement, learning and improvement in order to grow a strong customer base.

In his letter to shareholders back in 2016, Jeff Bezos said the following:  

>*Most decisions should probably be made with somewhere around 70% of the information you wish you had. If you wait for 90%, in most cases, you’re probably being slow. Plus, either way, you need to be good at quickly recognizing and correcting bad decisions. If you’re good at course correcting, being wrong may be less costly than you think, whereas being slow is going to be expensive for sure.*  

Jeff Bezos’ words apply to any product development in its early stages. The key is to be able to separate the ideal image you have about your product from its **core value** offering. This will help you get to market faster and **validate your idea** with a lower investment.

**Keep in mind:**
1. Building a successful MVP is the best way to fund the full project.
2. Avoid paying excessive development costs by keeping things minimal.
3. Get your first paying clients. This first iteration will give you the financial boost for the next phase.


You might also be interested in ["The Number One Reason Why Startups Fail"](https://viperdev.io/startup/2018/04/04/the-number-one-reason-why-startups-fail). Let us know what you think and visit us at [viperdev.io](https://viperdev.io/).
