(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.carousel').carousel();
    $(document).ready(function(){
      $('.carousel-class').slick({
        arrows: true,
        dots:true,
        customPaging : function(slider, i) {
          var title = $(slider.$slides[i]).data('title');
          return '<a class="pager__item"> '+title+' </a>';
        }
      });
    });
  }); // end of document ready
})(jQuery); // end of jQuery name space